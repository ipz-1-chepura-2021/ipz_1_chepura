import request from '../request.js'


export default {
    state: {
        userData: {},
    },
    mutations: {
        SET_USER_DATA(state, data){
            state.userData = data
        }
    },
    actions: {
        async register({ commit }, data){
            const response = await request("/user/registration", "POST", data)

            return response
        },
        async login({ commit }, data){
  
            const response = await request("/user/login", "POST", data)
 
            return response
        },
        async logout(){
            await request("/user/logout", "POST")
            localStorage.clear()
            window.location.reload()
        }
    }
}
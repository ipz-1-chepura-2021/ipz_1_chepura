import request from '../request.js'

export default {
    namespaced: true,
    state: {
        courses: [],
        error: ''
    },
    mutations: {
        ADD_COURSE(state, data){
            state.courses.unshift(data)
            state.error = ''
        },
        ERROR(state, error){
            state.error = error
        },
        UPDATE_COURSES(state, courses){
            state.courses = courses.reverse()
        }
    },
    actions: {
        async createCourse({ commit }, data){

            const response = await request("/course/", "POST", data)

            console.log(response)

            if(response.message){
                return commit('ERROR', response.message)
            }

            commit('ADD_COURSE', response)
            
        },
        async fetchCourses({ commit }){
            const response = await request("/course/", "GET")
            commit('UPDATE_COURSES', response)
        }
    },
    getters: {
        getCourseData: (state) => (id) => {
            const courses = state.courses

            return courses.filter((course) => course._id == id)[0]
        }
    }
}
import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login.vue'
import guards from './guards.js'

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/app',
    name: 'App',
    component: () => import('../views/App.vue'),
    children: [
      {
        path: "/app/createCourse",
        name: "CreateCourse",
        component: () => import('../components/createCourse.vue')
      },
      {
        path: "/app/coursesList",
        name: "CoursesList",
        component: () => import('../components/coursesList.vue')
      },
      {
        path: "/app/course/:ID",
        name: "CoursePage",
        component: () => import('../components/coursePage.vue')
      }
    ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default guards(router)

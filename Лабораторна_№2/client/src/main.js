import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/styles/general.css'



const create = () => {
    createApp(App).use(store).use(router).mount('#app')
}

if(localStorage.getItem('token')){
    const actions = [
        store.dispatch('course/fetchCourses')
    ]
        
    Promise.all(actions)
    .then(() => {
        create()
    })
}else{
    create()
}
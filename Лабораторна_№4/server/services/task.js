import taskModel from '../models/task.js'

class taskService{
    async create(course, text){

        const task = await taskModel.create({courseID: course, text, date: Date.now()})

        return await task.save()

    }
    async getCourse(courseID){
        console.log(courseID)
        const tasks = await taskModel.find({courseID})

        return tasks

    }
}

export default new taskService
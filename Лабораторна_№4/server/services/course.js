import courseModel from '../models/course.js'
import ApiError from '../exeptions/api-error.js'
import userModel from '../models/user.js'
import nodemailer from 'nodemailer'

class courseService{

    codeSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    codeLength = 6;

    async create(teacher, name, subject, description){

        const code = await this.generateUniqueCode()

        const course = await courseModel.create({teacher, name, subject, description,code})

        return await course.save()
        
    }
    async getForTeacher(id){
        const courses = await courseModel.find({teacher: id})
        
        return courses
    }
    generateCode(){
        let code = "";
        const possible = this.codeSymbols;
      
        for (var i = 0; i < this.codeLength; i++)
            code += possible.charAt(Math.floor(Math.random() * possible.length));
      
        return code;
    }
    async generateUniqueCode(){

        let isUnique = true
        let code = ''

        do{

            code = this.generateCode()

            const course = await courseModel.findOne({code})

            isUnique = course != null ? false : true

        }while(!isUnique)


        return code
            
    }
    async join(userID, code){
        const course = await courseModel.findOne({code})
        
        if(course == null){
            throw ApiError.BadRequest(`Invalid code`)
        }

        const user = await userModel.findById(userID)

        const courses = user.courses

        for (let i = 0; i < courses.length; i++) {
            if(courses[i] == course._id){
                return course._id 
            }
        }

        user.courses = [...courses, course._id]

        await user.save()

        return course._id
    }
    async getCoursesForStudent(id){

        const student = await userModel.findById(id)

        if(student == null){
            throw ApiError.BadRequest()
        }

        const courses = student.courses

        const response = []

        for (let i = 0; i < courses.length; i++) {
            const id = courses[i]

            const course = await courseModel.findById(id)

            if(course == null) continue

            response.push(course)
            
        }

        return response

    }
    async pass(text, teacher){

        const user = await userModel.findById(teacher)

        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
              user: 'chepuralearningsystem@gmail.com', 
              pass: '123kbgnjy123',
            },
        })

        let info = await transporter.sendMail({
            from: 'chepuralearningsystem@gmail.com',
            to: user.email,
            subject: "New answer", 
            text: "New answer", 
            html: "<p>"+ text +"</p>",
          })


        
    }
}

export default new courseService
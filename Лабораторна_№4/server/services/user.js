import UserModel from '../models/user.js'
import bcrypt from 'bcrypt'

import tokenService from './token.js'
import userDto from '../dtos/user.js'
import ApiError from '../exeptions/api-error.js'

import UserDto from '../dtos/user.js'

class User{
    async registration(login,password,firstName,lastName,role,email){
        const candidate = await UserModel.findOne({login})
        if(candidate){
            throw ApiError.BadRequest(`User already exists`)
        }
        const hashPassword = await bcrypt.hash(password, 3)
    
        const user = await UserModel.create({login, password:hashPassword, firstName,lastName,role,email})
       
        const userData = new userDto(user)
        const tokens = tokenService.generateTokens({...userData})

        await tokenService.save(userData.id, tokens.refreshToken)
        return {...tokens, user: userData}
    }
    async login(login, password){
        const candidate = await UserModel.findOne({login})

        console.log(login, password)

        if(!candidate) throw ApiError.BadRequest(`User with this login does not exist`)

        const isCorrectPassword = await bcrypt.compare(password,candidate.password)

        if(!isCorrectPassword) throw ApiError.BadRequest(`Wrong password`)

        const userData = new userDto(candidate)

        const tokens = tokenService.generateTokens({...userData})

        await tokenService.save(userData.id, tokens.refreshToken)

        return {...tokens, user: userData}

    }
    async logout(refreshToken){
        const token = tokenService.remove(refreshToken)
        return token
    }
    async getUserData(id){
        const data = await UserModel.findById(id)

        return new UserDto(data)
    }
}   
export default new User()
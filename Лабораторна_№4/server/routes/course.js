import {Router} from 'express'
import {body} from 'express-validator'
import auth from '../middlewares/auth.js'
import isTeacher from '../middlewares/isTeacher.js'
import isStudent from '../middlewares/isStudent.js'
import course from '../controllers/course.js'

const router = new Router()
router.post('/',auth, isTeacher,
    body('name').isLength({min: 2, max: 32}),
    body('subject').isLength({max: 999}),
    course.create)

router.post('/join',auth, isStudent, course.join)
router.post('/pass',auth, isStudent, course.pass)
router.get('/',auth, isTeacher,course.getForTeacher)
router.get('/student',auth, isStudent,course.getForStudent)

export default router
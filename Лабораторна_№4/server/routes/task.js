import {Router} from 'express'
import auth from '../middlewares/auth.js'
import isTeacher from '../middlewares/isTeacher.js'
import task from '../controllers/task.js'
const router = new Router()

router.post('/',auth,isTeacher, task.create)
router.post('/get',auth,task.getTasks)


export default router
export default class UserDto{
    login;
    firstName;
    lastName;
    role;
    _id;

    constructor(model){
        this.login = model.login
        this.firstName = model.firstName
        this.lastName = model.lastName
        this.role = model.role
        this._id = model._id
    }
}
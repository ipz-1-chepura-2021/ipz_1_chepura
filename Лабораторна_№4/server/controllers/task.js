
import taskService from '../services/task.js'

class taskController{
    async create(req, res, next){
        try{

            const response = await taskService.create(req.body.course, req.body.text)

            return res.status(200).json(response)

        }catch(e){
            next(e)
        }
    }
    async getTasks(req, res, next){
        try{

            const id = req.body.courseID

            const response = await taskService.getCourse(id)

            return res.status(200).json(response)

        }catch(e){
            next(e)
        }
    }
}


export default new taskController
import userService from '../services/user.js'
import {validationResult} from 'express-validator'
import ApiError from '../exeptions/api-error.js'

class User{
    async registration(req, res, next){
        try{
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                return next(ApiError.BadRequest('Validation error', errors.array()))
            }
            console.log(req.body)
            const {login, password, firstName, lastName, role, email} = req.body
            const userData = await userService.registration(login,password,firstName,lastName,role,email)

            res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})

            return res.json(userData)

        }catch(e){
            next(e)
        }
    }
    async login(req, res, next){
        try{
            console.log(req.body)

            const {login, password} = req.body
            const userData = await userService.login(login,password)

            res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})

            return res.json(userData) 
        }catch(e){
            next(e)
        }
    }
    async logout(req, res, next){
        try{
            const {refreshToken} = req.cookies
            const token = await userService.logout(refreshToken)
            res.clearCookie('refreshToken')
            return res.json(token)
        }catch(e){
            next(e)
        }
    }
}

export default new User()
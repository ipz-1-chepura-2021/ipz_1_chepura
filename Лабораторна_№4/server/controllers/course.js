import courseService from '../services/course.js'
import { validationResult } from 'express-validator'
import ApiError from '../exeptions/api-error.js'

class courseController{
    async create(req, res, next){
        try{

            const errors = validationResult(req)
            if(!errors.isEmpty()){
                return next(ApiError.BadRequest('Validation error', errors.array()))
            }

            const { name, subject, description } = req.body

            const response = await courseService.create(req.user._id, name, subject, description)

            return res.json(response).status(200)

        }catch(e){
            next(e)
        }
    }
    async getForTeacher(req, res, next){
        try{
            const id = req.user._id

            const response = await courseService.getForTeacher(id)

            return res.json(response).status(200)

        }catch(e){
            next(e)
        }
    }
    async join(req, res, next){
        try{
            const code = req.body.code
            const user = req.user._id

            const response = await courseService.join(user,code)

            return res.json(response).status(200)

        }catch(e){
            next(e)
        }
    }
    async getForStudent(req, res, next){
        try{
            const user = req.user._id

            const response = await courseService.getCoursesForStudent(user)

            return res.json(response).status(200)

        }catch(e){
            next(e)
        }
    }
    async pass(req, res, next){

        await courseService.pass(req.body.text, req.body.teacher)
        res.status(200)

    }
}


export default new courseController
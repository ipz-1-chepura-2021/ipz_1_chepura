import pkg from 'mongoose'
const { Schema, model } = pkg

const task = new Schema({
    courseID: {
        type: Schema.Types.ObjectId,
        ref: 'Course',
        required: true
    },
    text: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: new Date().now
    }
})

export default model('Task', task)
import pkg from 'mongoose'
const { Schema, model } = pkg

const user = new Schema({
    login:{
        type: String,
        unique: true,
        reqired: true
    },
    email: {
        type: String,
    },
    password:{
        type: String,
        reqired: true
    },
    firstName: {
        type: String,
        reqired: true
    },
    lastName: {
        type: String,
        reqired: true
    },
    role: {
        type: String,
        required: false,
        default: "Student"
    },
    courses: {
        type: Array,
        required: false,
        default: []
    }
})

export default model('User', user)
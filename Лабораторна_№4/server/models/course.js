import pkg from 'mongoose'
const { Schema, model } = pkg

const course = new Schema({
    teacher:{
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    name:{
        type: String,
        reqired: true
    },
    subject: {
        type: String,
        reqired: true
    },
    description: {
        type: String,
        reqired: false,
        default: ''
    },
    code: {
        type: String,
        reqired: true
    }
})

export default model('Course', course)
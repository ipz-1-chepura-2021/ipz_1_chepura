const token = localStorage.getItem('token')




export default function(router){

    const userData = localStorage.getItem('userData')
    let role = "Student"
    if(userData != null){
        role = JSON.parse(userData).role
    }


    router.beforeEach((to, from, next) => {
        if (to.href == '/') next({ name: 'App' })
        else next()
    })

    router.beforeEach((to, from, next) => {
        if ((to.name == 'Login' || to.name == 'Register') && token != null) next({ name: 'App' })
        else next()
    })

    
    router.beforeEach((to, from, next) => {
        switch (role) {
            case 'Teacher':
                if(to.name == "JoinCourse"){
                    next({ name: 'CreateCourse' })
                }
                break;
        
            case 'Student':
                if(to.name == "CreateCourse"){
                    next({ name: 'JoinCourse' })
                }
                break;
        }
        next()
    })

    router.beforeEach((to, from, next) => {
        const isApp = to.href.split('/')[1] === 'app' ? true : false
        if (isApp && token == null) next({ name: 'Login' })
        else next()
    })

    return router
}
import axios from 'axios'

const API_LINK = "http://localhost:3000/api"
axios.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    if(token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
    },(err) => {
        return Promise.reject(err);
});


const request = async (link, method, data = {}, headers = {'Content-Type': 'application/json'}) => {
    const response = await axios({
        method,
        data,
        url: API_LINK+link,
        headers,
    })
    return response.data
    
}

export default request
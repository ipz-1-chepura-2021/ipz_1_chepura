import { createStore } from 'vuex'
import user from './modules/user.js'
import course from './modules/course.js'
import task from './modules/task.js'


export default createStore({
  modules: {
    user,
    course,
    task
  }
})

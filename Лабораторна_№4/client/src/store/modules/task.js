import request from '../request.js'

export default {
    namespaced: true,
    state: {
        error: '',
        tasks: [],
        loading: true
    },
    mutations: {
        ERROR(state, error){
            state.error = error
        },
        ADD_TASK(state, data){
            state.tasks.unshift(data)
        },
        SET_TASKS(state, data){
            state.tasks = data.reverse()
        },
        LOADING(state, status){
            state.loading = status
        }
    },
    actions: {
        async create({ commit }, data){

            const response = await request("/task/", "POST", {text: data.text,course: data.course})

            if(response.message){
                return commit('ERROR', response.message)
            }

            commit("ADD_TASK", response)
        },
        async fetchTasks({ commit }, courseID){
            const response = await request("/task/get", "POST", {courseID})
            commit("LOADING", false)
            return response
        }
    },
    getters: {
      
    }
}
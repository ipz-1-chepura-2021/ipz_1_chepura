import request from "../../store/request.js"


export default {
    namespaced: true,
    state: {
        courses: [],
        error: ''
    },
    mutations: {
      SET_COURSES(state, data){
          state.courses = data
      },
      ADD_COURSE(state, data){
          if(state.courses.includes(data)) return 
          state.courses.unshift(data)
      },
      ERROR(state, error){
          state.error = error
      }
    },
    actions: {
        async join( { commit }, code ){

            const response = await request("/course/join/", "POST", { code })

            if(response.message){
                return commit("ERROR", response.message)
            }

            commit("ADD_COURSE", response)

            return response

        },
        async fetchCourses({ commit }){
            const response = await request("/course/student", "GET")

            commit("SET_COURSES", response)
        },
        async fetchTasks({ commit }, courseID){
            const response = await request("/task/get", "POST", {courseID})
            return response
        },
        async pass({ commit }, data){
            await request("/course/pass", "POST", data)
            
        }
    },
    getters: {
        getCourseData: (state) => (id) => {
            const courses = state.courses

            return courses.filter((course) => course._id == id)[0]
        }
    }
}
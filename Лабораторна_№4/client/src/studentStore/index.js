import { createStore } from 'vuex'
import user from '../store/modules/user.js'
import course from './modules/course.js'


export default createStore({
  modules: {
    user,
    course
  }
})
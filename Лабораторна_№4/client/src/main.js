import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import teacherStore from './store'
import studentStore from './studentStore'
import './assets/styles/general.css'



const token = localStorage.getItem('token')

let userData = localStorage.getItem('userData')
let role = "Student"
if(userData != null){
    role = JSON.parse(userData).role
}


const create = () => {
    
    let store = {}


    switch (role) {
        case 'Teacher':
            store = teacherStore
            break;
    
        case "Student":
            store = studentStore
            break;
    }

    createApp(App).use(store).use(router).mount('#app')
}

if(token){

    let actions = []

    switch (role) {
        case "Teacher":
            actions = [
                teacherStore.dispatch('course/fetchCourses'),
            ]
            break;
    
        case "Student":

            actions = [
                studentStore.dispatch('course/fetchCourses'),
            ]

            break;
        default:
            localStorage.clear()
    }
        
    Promise.all(actions)
    .then(() => {
        create()
    })
}else{
    create()
}
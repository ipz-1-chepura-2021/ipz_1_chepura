import courseService from '../services/course.js'
import { validationResult } from 'express-validator'
import ApiError from '../exeptions/api-error.js'

class courseController{
    async create(req, res, next){
        try{

            const errors = validationResult(req)
            if(!errors.isEmpty()){
                return next(ApiError.BadRequest('Validation error', errors.array()))
            }

            const { name, subject, description } = req.body

            const response = await courseService.create(req.user._id, name, subject, description)

            return res.json(response).status(200)

        }catch(e){
            next(e)
        }
    }
    async getForTeacher(req, res, next){
        try{
            const id = req.user._id

            const response = await courseService.getForTeacher(id)

            return res.json(response).status(200)

        }catch(e){
            next(e)
        }
    }
}


export default new courseController
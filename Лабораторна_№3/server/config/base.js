export default {
    PORT: process.env.PORT || 3000,
    API_URL: "http://localhost:3000",
    CLIENT_URL: "http://localhost:8080"
}
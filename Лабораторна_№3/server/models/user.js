import pkg from 'mongoose'
const { Schema, model } = pkg

const user = new Schema({
    login:{
        type: String,
        unique: true,
        reqired: true
    },
    password:{
        type: String,
        reqired: true
    },
    firstName: {
        type: String,
        reqired: true
    },
    lastName: {
        type: String,
        reqired: true
    },
    role: {
        type: String,
        required: false,
        default: "Student"
    }
})

export default model('User', user)
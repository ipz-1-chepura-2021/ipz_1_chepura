import ApiError from '../exeptions/api-error.js'

export default function (req, res, next) {
    try{
        if(req.user.role != "Teacher"){
            return next(ApiError.AccessDenied())
        }
        next()

    }catch(e){
        next(ApiError.UnautorizedError())
    }
}
import express from 'express'
import cors from 'cors'
import {base, db} from './config/index.js'
import mongoose from 'mongoose'
import history from 'connect-history-api-fallback'
import cookieParser from 'cookie-parser'
import userRouter from './routes/user.js'
import courseRouter from './routes/course.js'
import errors from './middlewares/errors.js'
import bodyParser from 'body-parser'
const app = express()

app.use(bodyParser.json())
app.use(cookieParser())
app.use(cors())
app.use('/api/user', userRouter)
app.use('/api/course', courseRouter)
app.use(history())
app.use(errors)

const start = async () => {
    try {
        await mongoose.connect(db.dbLink, {
            useNewUrlParser: true,
            useFindAndModify: false
        })
        app.listen(base.PORT, (err)=>{
            if(err){
                throw err
            }else{
                console.log(`Server has been started on port ${base.PORT}`)
            }
        })
    } catch (error) {
        throw error
    }
}

start()
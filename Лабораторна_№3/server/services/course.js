import courseModel from '../models/course.js'


class courseService{

    codeSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    codeLength = 6;

    async create(teacher, name, subject, description){

        const code = await this.generateUniqueCode()

        const course = await courseModel.create({teacher, name, subject, description,code})

        return await course.save()
        
    }
    async getForTeacher(id){
        const courses = await courseModel.find({teacher: id})
        
        return courses
    }
    generateCode(){
        let code = "";
        const possible = this.codeSymbols;
      
        for (var i = 0; i < this.codeLength; i++)
            code += possible.charAt(Math.floor(Math.random() * possible.length));
      
        return code;
    }
    async generateUniqueCode(){

        let isUnique = true
        let code = ''

        do{

            code = this.generateCode()

            const course = await courseModel.findOne({code})

            isUnique = course != null ? false : true

        }while(!isUnique)


        return code
            
    }
}

export default new courseService
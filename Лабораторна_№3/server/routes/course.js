import {Router} from 'express'
import {body} from 'express-validator'
import auth from '../middlewares/auth.js'
import isTeacher from '../middlewares/isTeacher.js'
import course from '../controllers/course.js'

const router = new Router()
router.post('/',auth, isTeacher,
    body('name').isLength({min: 2, max: 32}),
    body('subject').isLength({max: 32}),
    course.create)
router.get('/',auth, isTeacher,course.getForTeacher)

    


export default router
import {Router} from 'express'
import user from '../controllers/user.js'
import {body} from 'express-validator'
import auth from '../middlewares/auth.js'

const router = new Router()



router.post('/registration', 
    body('login').isLength({min: 3, max: 16}),
    body('password').isLength({min: 6, max: 32}),
    body('firstName').isLength({min: 3, max: 32}),
    body('lastName').isLength({min: 3, max: 32}),
    body('role').isLength({min: 6, max: 32})
,user.registration)
router.post('/login', user.login)
router.post('/logout', user.logout)

export default router